<?php

namespace Cf\EnvInfo\Block;


use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;


/**
 * Class Info
 */
class Info extends \Magento\Framework\View\Element\Template
{


    /** @var string */
    protected $env;

    /**
     * Constructor
     *
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\DeploymentConfig $deploymentConfig,
        Context $context,
        array $data = []
    )
    {
        $this->env = strtoupper((trim((string)$deploymentConfig->get('env/id'))));
        parent::__construct($context, $data);
    }


    /**
     * @return string
     */
    public function getEnvClass()
    {
        return strtolower($this->getEnv());
    }


    /**
     * @return string
     */
    public function getEnv()
    {
        return (string) $this->env;
    }


}
